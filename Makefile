build:
	cargo lambda build --release

deploy: build
	cargo lambda deploy --iam-role 