use std::str;

use futures::stream::TryStreamExt;
use mongodb::{
    bson::{doc, oid::ObjectId},
    error::Error,
    Database,
};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssociationModel {
    pub _id: ObjectId,
    pub name: String,
    pub description: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Association {
    pub id: String,
    pub name: String,
    pub description: String,
    pub events: Vec<Event>,
    pub members: Vec<Member>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EventModel {
    pub _id: ObjectId,
    pub association_id: ObjectId,
    pub name: String,
    pub description: String,
    pub participants: Vec<ObjectId>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Event {
    pub id: String,
    pub name: String,
    pub description: String,
    pub participants: Vec<Member>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MemberModel {
    pub _id: ObjectId,
    pub association_id: ObjectId,
    pub email: String,
    pub role: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Member {
    pub id: String,
    pub email: String,
    pub role: String,
}

pub async fn get_associations(db: Database) -> Result<Vec<Association>, Error> {
    let collection = db.collection::<AssociationModel>("associations");
    let mut cursor = collection.find(None, None).await?;
    let mut associations: Vec<Association> = vec![];

    while let Some(doc) = cursor.try_next().await? {
        let association = Association {
            id: doc._id.to_hex(),
            name: doc.name,
            description: doc.description,
            events: get_events_from_associations(db.clone(), doc._id).await?,
            members: get_members_from_associations(db.clone(), doc._id).await?,
        };
        associations.push(association)
    }

    Ok(associations)
}

pub async fn get_members_from_associations(
    db: Database,
    id: ObjectId,
) -> Result<Vec<Member>, Error> {
    let collection = db.collection::<MemberModel>("members");

    let filter = doc! { "association_id": id };

    let mut cursor = collection.find(filter, None).await?;
    let mut members: Vec<Member> = vec![];

    while let Some(doc) = cursor.try_next().await? {
        let member = Member {
            id: doc._id.to_hex(),
            email: doc.email,
            role: doc.role,
        };
        members.push(member)
    }

    Ok(members)
}

pub async fn get_events_from_associations(db: Database, id: ObjectId) -> Result<Vec<Event>, Error> {
    let collection = db.collection::<EventModel>("events");

    let filter = doc! { "association_id": id };

    let mut cursor = collection.find(filter, None).await?;
    let mut events: Vec<Event> = vec![];

    while let Some(doc) = cursor.try_next().await? {
        let event = Event {
            id: doc._id.to_hex(),
            name: doc.name,
            description: doc.description,
            participants: get_members_by_ids(db.clone(), doc.participants).await?,
        };
        events.push(event)
    }

    Ok(events)
}

pub async fn get_members_by_ids(db: Database, ids: Vec<ObjectId>) -> Result<Vec<Member>, Error> {
    let collection = db.collection::<MemberModel>("members");

    let filter = doc! { "_id": { "$in": ids } };

    let mut cursor = collection.find(filter, None).await?;
    let mut members: Vec<Member> = vec![];

    while let Some(doc) = cursor.try_next().await? {
        let member = Member {
            id: doc._id.to_hex(),
            email: doc.email,
            role: doc.role,
        };
        members.push(member)
    }

    Ok(members)
}
