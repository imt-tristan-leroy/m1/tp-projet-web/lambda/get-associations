mod association;

use association::{get_associations, Association};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use mongodb::Client;
use serde::Serialize;

#[derive(Serialize)]
struct ResponseBody {
    content: Vec<Association>,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(_event: Request) -> Result<Response<Body>, Error> {
    let mongo_uri = "mongodb://localhost:27017";
    let mongo_database = "test"; //"kairos";

    let client = Client::with_uri_str(mongo_uri).await?;
    let database = client.database(mongo_database);

    let associations = get_associations(database).await?;

    let resp = Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&associations)?.into())
        .map_err(Box::new)?;
    return Ok(resp);
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
